import { EventEmitter } from "events";
import {EventsEnum } from './events-enum';
//NewEmitter class ochib EventEmitterdan meros olish
class NewEmitter extends EventEmitter{}
 //NewEmitter dan instance(andoza) olish
 const newEmitter = new NewEmitter();
//event listener
 newEmitter.on(EventsEnum.HELLO,()=> {
    console.log('Hello World')
})
newEmitter.on(EventsEnum.BYE,()=> {
    console.log('Bye World')
})
 //qabul qilib olish
 newEmitter.emit(EventsEnum.HELLO)
 newEmitter.emit(EventsEnum.BYE)